package com.test;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class ApiTestAutomation {
	@Test
	public void getCitiesDetails() {
		RestAssured.baseURI="uri";
		RequestSpecification httpRequest=RestAssured.given();
		Response response=httpRequest.request(Method.GET, "");
		String responseBody=response.getBody().asString();
		System.out.println("Response: "+responseBody);
		int statusCode=response.getStatusCode();
		Assert.assertEquals(200, statusCode);
		
	}

}
